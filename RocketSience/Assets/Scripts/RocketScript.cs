﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RocketScript : MonoBehaviour
{
    Rigidbody rocketRigidBody;
    AudioSource rocketAudioSource;
    [SerializeField] float rcsThrust = 100f;
    [SerializeField] float mainThrust = 100f;
    [SerializeField] float levelLoadDelay = 2f;

    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip deathSound;
    [SerializeField] AudioClip victorySound;

    [SerializeField] ParticleSystem engineParticle;
    [SerializeField] ParticleSystem successParticle;
    [SerializeField] ParticleSystem deathParticle;

    enum State { Alive, Dying, Transcending }
    State state = State.Alive;

    void Start()
    {
        rocketRigidBody = GetComponent<Rigidbody>();
        rocketAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (state == State.Alive)
        {
            RespondToRotationInput();
            RespondToThrustInput();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (state != State.Alive)
            return;

        switch (collision.gameObject.tag)
        {
            case "Friendly":
                break;
            case "Deadly":
                StartDeathSequence();
                break;
            case "Finish":
                StartSuccessSequence();

                break;
            default:
                break;
        }
    }

    private void StartDeathSequence()
    {
        state = State.Dying;
        rocketAudioSource.Stop();
        rocketAudioSource.PlayOneShot(deathSound);
        deathParticle.Play();
        Invoke("LoadFirstLevel", levelLoadDelay);
    }

    private void StartSuccessSequence()
    {
        state = State.Transcending;
        rocketAudioSource.Stop();
        rocketAudioSource.PlayOneShot(victorySound);
        successParticle.Play();
        Invoke("LoadNextLevel", levelLoadDelay);
    }

    private void LoadFirstLevel()
    {
        SceneManager.LoadScene(0);
    }

    private void LoadNextLevel()
    {
        successParticle.Play();
        SceneManager.LoadScene(1);
    }

    private void RespondToThrustInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rocketAudioSource.PlayOneShot(mainEngine);
            engineParticle.Play();
        }

        if (Input.GetKey(KeyCode.Space))
            ApplyThrust();

        if (Input.GetKeyUp(KeyCode.Space))
        {
            engineParticle.Stop();
            rocketAudioSource.Stop();
        }
    }

    private void ApplyThrust()
    {
        float mainThrustSpeedThisFrame = mainThrust * Time.deltaTime;
        rocketRigidBody.AddRelativeForce(Vector3.up * mainThrustSpeedThisFrame);
    }

    private void RespondToRotationInput()
    {

        rocketRigidBody.freezeRotation = true;

        float rotationSpeedThisFrame = rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            
            transform.Rotate(Vector3.forward * rotationSpeedThisFrame);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.back * rotationSpeedThisFrame);
        }

        rocketRigidBody.freezeRotation = false;
    }
}
